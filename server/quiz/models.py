from django.db import models

class Quiz(models.Model):
    title=models.CharField(max_length=200)
    slug=models.SlugField()
    published=models.DateTimeField(auto_now_add=True)
    start_at=models.DateTimeField()
    end_at=models.DateTimeField()

class Take(models.Model):
    user=models.ForeignKey(User, on_delete=models.CASCADE)
    quiz=models.ForeignKey(Quiz, on_delete=models.CASCADE)
    score = models.SmallIntegerField()
    start_at=models.DateTimeField()
    end_at=models.DateTimeField()

class QuizQuestion(models.Model):
    quiz=models.ForeignKey(Quiz, on_delete=models.CASCADE)
    level=models.SmallIntegerField()
    score=models.SmallIntegerField()
    created_at=models.DateTimeField(auto_now_add=True)
    updated_at=models.DateTimeField(auto_now_add=True)
    content=models.TextField()


class QuizAnswer(models.Model):
    quiz=models.ForeignKey(Quiz, on_delete=models.CASCADE)
    quistion=models.ForeignKey(QuizQuestion, on_delete=models.CASCADE)
    corect=models.BooleanField(default=False)
    created_at=models.DateTimeField(auto_now_add=True)
    updated_at=models.DateTimeField(auto_now_add=True)
    content=models.TextField()


class TakeAnswer(models.Model):
    take=models.ForeignKey(Take, on_delete=models.CASCADE)
    question=models.ForeignKey(QuizQuestion, on_delete=models.CASCADE)
    answer=models.ForeignKey(QuizAnswer, on_delete=models.CASCADE)
    created_at=models.DateTimeField(auto_now_add=True)
    updated_at=models.DateTimeField(auto_now_add=True)
    